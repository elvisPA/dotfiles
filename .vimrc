call plug#begin('~/.vim/plugged')
Plug 'morhetz/gruvbox'
Plug 'nanotech/jellybeans.vim'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'junegunn/goyo.vim', { 'on':  'Goyo' }
Plug 'junegunn/LimeLight.vim', { 'on':  'Limelight' }
Plug 'itchyny/lightline.vim'
Plug 'airblade/vim-gitgutter'
Plug 'vim-scripts/a.vim'
Plug 'tpope/vim-fugitive'
Plug 'bling/vim-bufferline'
Plug 'Valloric/YouCompleteMe', { 'do': './install.py --clang-completer' }
Plug 'rdnetto/YCM-Generator', { 'branch': 'stable'}
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'sheerun/vim-polyglot'
Plug 'Yggdroot/indentLine'
Plug 'majutsushi/tagbar'
call plug#end()

"< general
syntax enable
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set cursorline
set number
set foldmethod=syntax
set nofoldenable
set t_Co=256
set t_ut= " hack for background
set timeoutlen=1000 ttimeoutlen=0 " escape delay

filetype indent on " load filetype-specific indent files
set wildmenu " visual autocomplete for command menu
set showmatch " highlight matching [{()}]
set incsearch " search as characters are entered
set hlsearch " highlight matches
nnoremap <leader><space> :nohlsearch<CR>
" set colorcolumn=80
highlight ColorColumn ctermbg=darkgray
autocmd BufNewFile,BufReadPost *.ino,*.pde set filetype=cpp
"> general

"< Goyo
let g:goyo_width = 140

">

"< LimeLight 
let g:limelight_conceal_ctermfg = 'gray'

autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!
"> LimeLight


"< gruvbox
let g:gruvbox_italic=1
set background=dark
colorscheme gruvbox
"> gruvbox

"< jellybeans
"colorscheme jellybeans
"> jellybeans

"< NERDTtree
let NERDTreeShowHidden=1
let NERDTreeShowBookmarks=1
let NERDTreeIgnore=['\.DS_Store', '\~$', '\.swp']
nmap <F7> :NERDTreeToggle<CR>
">

"< YCM
let g:ycm_global_ycm_extra_conf='/home/adaszek/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py'
let g:ycm_confirm_extra_conf = 0
let g:ycm_always_populate_location_list=1
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
nnoremap <leader>h :YcmCompleter GoToDeclaration<CR>
nnoremap <leader>d :YcmCompleter GoToDefinitionElseDeclaration<CR>
nnoremap <leader>i :YcmCompleter GoToDefinition<CR>
"> 

"< ultisnips
" Trigger configuration. Do not use <tab> if you use
" https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<leader><tab>c"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

 " If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"
">
" Vim
let g:indentLine_color_term = 239
"
" GVim
let g:indentLine_color_gui = '#A4E57E'
"
" none X terminal
let g:indentLine_color_tty_light = 7 " (default: 4)
let g:indentLine_color_dark = 1 " (default: 2)
"< indentLine
" < tagbar
nmap <F8> :TagbarToggle<CR>
">
